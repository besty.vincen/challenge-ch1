import java.util.Scanner;

public class Challenge {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        new Challenge();

    }

    public Challenge() {
        // TODO Auto-generated constructor stub
        int input;
        do {
            System.out.println("Kalkulator Penghitung Luas dan Volume");
            System.out.println("Menu");
            System.out.println("1. Hitung Luas Bidang");
            System.out.println("2. Hitung Volum");
            System.out.println("0. Tutup Aplikasi");
            System.out.print(">>");

            input = sc.nextInt();
            int luas, volume;
            double luasLingkaran, volumeTabung;
            switch (input) {

                case 1:
                    switch (showMenuLuas()) {
                        case 1:
                            System.out.print("Masukkan panjang sisi = ");
                            int sisi = sc.nextInt();
                            luas = sisi*sisi;

                            System.out.println("Processing...");
                            System.out.println("Luas dari persegi adalah " + luas);
                            System.out.println();
                            break;

                        case 2:
                            System.out.print("Masukkan panjang jari-jari = ");
                            int r = sc.nextInt();
                            luasLingkaran = (3.14)*r*r;

                            System.out.println("Processing...");
                            System.out.println("Luas dari lingkaran adalah " + luasLingkaran);
                            System.out.println();
                            break;

                        case 3:
                            System.out.print("Masukkan panjang alas = ");
                            int a = sc.nextInt();
                            System.out.print("Masukkan tinggi = ");
                            int t = sc.nextInt();
                            luas = a*t/2;

                            System.out.println("Processing...");
                            System.out.println("Luas dari segitiga adalah " + luas);
                            System.out.println();
                            break;

                        case 4:
                            System.out.print("Masukkan panjang = ");
                            int p = sc.nextInt();
                            System.out.print("Masukkan lebar = ");
                            int l = sc.nextInt();
                            luas = p*l;

                            System.out.println("Processing...");
                            System.out.println("Luas dari persegi panjang adalah " + luas);
                            System.out.println();
                            break;

                        case 0:
                            break;

                    }
                    break;

                case 2:
                    switch (showMenuVolum()) {
                        case 1:
                            System.out.print("Masukkan panjang sisi = ");
                            int sisi = sc.nextInt();
                            volume = sisi*sisi*sisi;

                            System.out.println("Processing...");
                            System.out.println("Volume dari kubus adalah " + volume);
                            System.out.println();
                            break;

                        case 2:
                            System.out.print("Masukkan panjang = ");
                            int p = sc.nextInt();
                            System.out.print("Masukkan lebar = ");
                            int l = sc.nextInt();
                            System.out.print("Masukkan tinggi = ");
                            int t = sc.nextInt();
                            volume = p*l*t;

                            System.out.println("Processing...");
                            System.out.println("Volume dari balok adalah " + volume);
                            System.out.println();
                            break;

                        case 3:
                            System.out.print("Masukkan jari-jari = ");
                            int r = sc.nextInt();
                            System.out.print("Masukkan tinggi = ");
                            int t2 = sc.nextInt();
                            volumeTabung = (3.14)*r*r*t2;

                            System.out.println("Processing...");
                            System.out.println("Volume dari tabung adalah " + volumeTabung);
                            System.out.println();
                            break;

                        case 0:
                            break;
                    }
                    break;


            }
        } while (input!=0);
    }

    private static int showMenuLuas() {
        // TODO Auto-generated method stub
        System.out.println("Pilih bidang yang akan dihitung");
        System.out.println("1. persegi");
        System.out.println("2. lingkaran");
        System.out.println("3. segitiga");
        System.out.println("4. persegi panjang");
        System.out.println("0. kembali ke menu sebelumnya");
        System.out.print(">>");

        int input = sc.nextInt();
        sc.nextLine();
        return input;
    }
    private static int showMenuVolum() {
        // TODO Auto-generated method stub
        System.out.println("Pilih bidang yang akan dihitung");
        System.out.println("1. kubus");
        System.out.println("2. balok");
        System.out.println("3. tabung");
        System.out.println("0. kembali ke menu sebelumnya");
        System.out.print(">>");

        int input = sc.nextInt();
        sc.nextLine();
        return input;
    }


}

